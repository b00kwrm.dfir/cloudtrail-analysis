#!/bin/sh
# I got the command below from here:
# https://colab.research.google.com/github/google/picatrix/blob/main/notebooks/Quick_Primer_on_Colab_Jupyter.ipynb#scrollTo=rCRIzPZjde9x
# I am not sure what it does, but it looked important and I will look it up later.
jupyter serverextension enable --py jupyter_http_over_ws
