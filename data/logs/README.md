# Cloudtrail Cogs

These logs are from the Scott Piper at Summit Route. You can read more about them in the link below. 

Thank you, Scott!

Blog post: Public dataset of Cloudtrail logs from flaws.cloud
https://summitroute.com/blog/2020/10/09/public_dataset_of_cloudtrail_logs_from_flaws_cloud/

Direct Link to log files:
http://summitroute.com/downloads/flaws_cloudtrail_logs.tar


