[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/b00kwrm.dfir/cloudtrail-analysis)

# cloudtrail-analysis

This project is being created to do analysis on cloudtrail logs. The goal of the project is to learn more about cloudtrail, pandas, jupyter notebooks, timesketch and hopefully produce something useful.

See the wiki for a list of good links: https://gitlab.com/b00kwrm.dfir/cloudtrail-analysis/-/wikis/home
